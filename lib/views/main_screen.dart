import 'package:flutter/material.dart';
import 'package:iot_quickcheck/views/list_screen.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "IoT Quickcheck",
      home: Scaffold(
        appBar: AppBar(
          title: Text("IoT Quickcheck"),
        ),
        body: ListScreen(),
      ),
    );
  }
}