import 'package:flutter/material.dart';
import 'package:iot_quickcheck/bloc/shodan_bloc.dart';
import 'package:iot_quickcheck/bloc/shodan_state.dart';
import 'package:iot_quickcheck/models/shodan_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ListScreenState();
}

class ListScreenState extends State<ListScreen> {
  final TextEditingController _controller = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 90,
          child: Container(
              padding: EdgeInsets.all(10),
              child: Form(
                key: formKey,
                child: TextFormField(
                  onFieldSubmitted: (text) {
                    if (formKey.currentState.validate()) {
                      _focusNode.unfocus();
                      shodanBloc.sinkApiResponse(_controller.text);
                    }
                  },
                  controller: _controller,
                  validator: (text) =>
                      text == '' ? 'Pole nie może być puste' : null,
                  focusNode: _focusNode,
                  decoration: InputDecoration(
                      labelText: 'Nazwa firmy',
                      suffixIcon: IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () {
                          if (formKey.currentState.validate()) {
                            _focusNode.unfocus();
                            shodanBloc.sinkApiResponse(_controller.text);
                          }
                        },
                      )),
                ),
              )),
        ),
        Container(
          child: StreamBuilder(
            stream: shodanBloc.shodanSubject.stream,
            builder: (BuildContext context,
                AsyncSnapshot<ShodanState> snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    'Problem z połączeniem!',
                    style: TextStyle(fontSize: 18),
                  ),
                );
              }
              if (snapshot.hasData) {
                if(snapshot.data is ShodanLoading) {
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 100),
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Flexible(
                    child: SingleChildScrollView(
                      child: Container(
                        child: Column(
                          children: getList((snapshot.data as ShodanSuccess).response),
                        ),
                      ),
                    ),
                  );
                }
              } else {
                return Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 50),
                  child: Text(
                    'Wpisz nazwę firmy',
                    style: TextStyle(fontSize: 18),
                  ),
                );
              }
            },
          ),
        )
      ],
    );
  }

  List<Widget> getList(List<ShodanModel> list) {
    return List<Widget>.generate(list.length, (index) {
      return getListItem(list[index]);
    });
  }

  Widget getListItem(ShodanModel shodanModel) => Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        padding: EdgeInsets.all(10),
        color: Colors.black12,
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Text(
                'Filters: ${shodanModel.vulnerability}',
                style: TextStyle(fontSize: 16),
              ),
            ),
            Padding(
              child: Divider(
                height: 1,
                color: Colors.black,
              ),
              padding: EdgeInsets.all(10),
            ),
            Container(
              child: Text(
                'Adress IP: ${shodanModel.ipStr}',
              ),
            ),
            Center(
              child:
                  shodanModel.domains != null && shodanModel.domains.isNotEmpty
                      ? Text('Domain: ${shodanModel.domains[0]}')
                      : Text('Domain: -----'),
            ),
            Padding(
              child: Divider(
                height: 1,
                color: Colors.black,
              ),
              padding: EdgeInsets.all(10),
            ),
            Center(
              child: Text(shodanModel.location.countryName),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text('Latitude:'),
                    Text(shodanModel.location.latitude.toString()),
                  ],
                ),
                SizedBox(
                  width: 15,
                ),
                Column(
                  children: <Widget>[
                    Text('Longitude:'),
                    Text(shodanModel.location.longitude.toString()),
                  ],
                ),
              ],
            ),
          ],
        ),
      ));
}
