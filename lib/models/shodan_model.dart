

import 'package:iot_quickcheck/models/location_model.dart';

class ShodanModel {
  String ipStr;
  String data;
  LocationModel location;
  List<String> domains;
  String vulnerability;

  ShodanModel({this.data, this.domains, this.ipStr, this.location});

  factory ShodanModel.fromJson(Map<String, dynamic> json) {
    return ShodanModel(
      ipStr: json['ip_str'],
      data: json['data'],
      domains: List<String>.from(json['domains']),
      location: LocationModel.fromJson(json['location']),
    );
  }
}