

class LocationModel {
  double longitude;
  double latitude;
  String countryName;

  LocationModel({this.longitude, this.latitude, this.countryName});

  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return LocationModel(
      countryName: json['country_name'],
      longitude: json['longitude'],
      latitude: json['latitude']
    );
  }
}