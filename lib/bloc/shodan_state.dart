

import 'package:iot_quickcheck/models/shodan_model.dart';

class ShodanState {
  String name;

  ShodanState(this.name);
}

class ShodanLoading extends ShodanState {
  ShodanLoading(): super('ShodanLoading');
}

class ShodanSuccess extends ShodanState {
  List<ShodanModel> response;

  ShodanSuccess(this.response): super('ShodanSuccess');
}