import 'package:iot_quickcheck/bloc/shodan_state.dart';
import 'package:iot_quickcheck/models/shodan_model.dart';
import 'package:iot_quickcheck/repositories/shodan_repostory.dart';
import 'package:rxdart/rxdart.dart';

final shodanBloc = ShodanBloc._();

class ShodanBloc {
  ShodanBloc._();
  ShodanRepository repo = ShodanRepository();
  
  BehaviorSubject<ShodanState> _shodanSubject;

  BehaviorSubject<ShodanState> get shodanSubject {
    if(_shodanSubject == null) {
    _shodanSubject = BehaviorSubject<ShodanState>();
    }
    return _shodanSubject;
  }
  
  Future<void> sinkApiResponse(String company) async {
    shodanSubject.sink.add(ShodanLoading());
    var response = await repo.callApi(company);
    shodanSubject.sink.add(ShodanSuccess(response));
  }

  dispose() {
    if(_shodanSubject != null) _shodanSubject.close();
  }
}