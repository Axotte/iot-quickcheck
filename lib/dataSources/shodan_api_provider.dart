import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:iot_quickcheck/models/shodan_model.dart';

class ShodanApiProvider {
  static const String _apikey = "gQXGX9pngwYTEswKqsBHMoT2SAYpHyqP";
  static const String _baseUrl = "https://api.shodan.io/shodan/host/search?key=$_apikey";

  Future<List<ShodanModel>> callApi(String vulnerability, String org) async {
    try {
      String url = '$_baseUrl&query=$vulnerability org::"$org"';
      var response = await http.get(url);
      if(response.statusCode == 200) {
        print(response.body);
        Iterable iterable = json.decode(response.body)['matches'];
        return iterable.map((model) => ShodanModel.fromJson(model)..vulnerability = vulnerability).toList();
      } else {
        print(response.statusCode);
        print(response.body);
        return List<ShodanModel>();
      }
    } catch (e) {
      print(e);
      return List<ShodanModel>();
    }

  }
}