

import 'package:iot_quickcheck/dataSources/shodan_api_provider.dart';
import 'package:iot_quickcheck/models/shodan_model.dart';

class ShodanRepository {
  ShodanApiProvider _shodanApiProvider = ShodanApiProvider();
  Future<List<ShodanModel>> callApi(String org) async {
    List<ShodanModel> list = List<ShodanModel>();
    list.addAll(await _shodanApiProvider.callApi('device:printer', org));
    list.addAll(await _shodanApiProvider.callApi('title:"Sunny WebBox"', org));
    list.addAll(await _shodanApiProvider.callApi('http.title:wallbox', org));
    list.addAll(await _shodanApiProvider.callApi('port:5353 "Apple"', org));
    list.addAll(await _shodanApiProvider.callApi('vsftpd 2.3.4', org));
    return list;
  }
}